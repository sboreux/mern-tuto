import lodash from 'lodash';
import React from 'react';
require('codemirror/lib/codemirror.css');
require('codemirror/addon/fold/foldgutter.css')
require('codemirror/addon/fold/foldgutter.js');
require('codemirror/addon/fold/foldcode.js');
require('../codemirror/mode/mt.js');
require('../codemirror/mode/fold-mt.js');

var CodeMirror = require('react-codemirror');

export default class Editor extends React.Component  {

  constructor(props) {
      super(props);
      this.state =this.getInitialState();
    }

  getInitialState() {
		return {
			code: ":16R:GENL\r\n\
:20C::SEME//MY REFERENCE \r\n\
:23G:NEWM\r\n\
:16S:GENL\r\n\
:16R:TRADDET\r\n\
:98A::SETT//20110404\r\n\
:98A::TRAD//20110331\r\n\
:35B:ISIN BE0312668370\r\n\
TREASURY BILL\r\n\
:70E::SPRO//SEQN/67939\r\n\
:16S:TRADDET\r\n\
:16R:SETDET\r\n\
:22F::SETR/NBBE/15XX\r\n\
:16R:SETPRTY\r\n\
:95R::REAG/NBBE/0100\r\n\
:97A::SAFE//100801000267\r\n\
:16S:SETPRTY\r\n\
:16R:SETPRTY\r\n\
:95P::PSET//NBBEBEBB216\r\n\
:16S:SETPRTY\r\n\
:16S:SETDET",
		};
	}


	updateCode(newCode) {
		this.state.code = newCode;
	}

	render() {
		var options = {
			lineNumbers: true,
      mode:  "mt",
      foldGutter: true,
      gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"]
		};
		return <CodeMirror value={this.state.code} onChange={this.updateCode} options={options} />
	}

}
