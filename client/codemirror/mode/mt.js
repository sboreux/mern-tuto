var CodeMirror = require('codemirror');
import lodash from 'lodash';

CodeMirror.defineMode("mt", function() {

  function isSpecificTag(tag)
  {
    return _.includes([':23G:',':28E:',':35B:'],tag);
  }

  function isStartTag(tag)
  {
    return tag === ':16R:'
  }

  function isEndTag(tag)
  {
    return tag === ':16S:'
  }

function isISO15022 (state,tag)
{
  if ("undefined" === typeof state.iso15022)
  {
      state.iso15022 = isStartTag(tag);
  }
  return state.iso15022;
}
  function start(stream,state)
  {
    var tag = _.reduce(stream.match(/:\d{2}[A-Z]?:/), (a,b) => a+b);
    if (tag)
    {
      if (!isISO15022(state,tag) || isSpecificTag(tag) )
      {
        state.cur=content;
        return "stag";
      }
      if (isStartTag(tag) || isEndTag(tag))
      {
        state.cur=sequence;
        return "stag";
      }
      state.cur = generic;
      return "gtag";
    }
    state.cur = error;
    return null;
  }


  function generic(stream,state)
  {
    var qual = stream.match(/:[A-Z]{4}\//);
    if (qual)
    {
      state.cur = dss;
      return "qual";
    }
    state.cur = error;
    return null;
  }

  function dss(stream,state)
  {
    var qual = stream.match(/[A-Z]{1,8}\//);
    if (qual)
    {
      state.cur = content;
      return "dss";
    }
    state.cur = content;
    return null;
  }

  function content (stream,state)
  {
    if (!stream.sol())
    {
      stream.skipToEnd();
    }
    state.cur = start;
  }

  function sequence (stream,state)
  {
    if (!stream.sol())
    {
      stream.skipToEnd();
    }
    state.cur = start;
    return 'sequence';
  }

  function error (stream,state)
  {
    stream.skipToEnd();
    state.cur = start;
  }

  return {
    token: function(stream, state) {
      var cur = state.cur;
      return cur(stream, state);
    },
    startState: function() {
      return {cur: start};
    }
  };
});
