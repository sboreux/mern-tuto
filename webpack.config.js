const path = require('path');
const webpack = require('webpack');


module.exports = {
  //devtool: 'inline-source-map',
  entry: [
    'webpack-dev-server/client?http://127.0.0.1:8000/',
    'webpack/hot/only-dev-server',
    './client'],
  output:  {
    path: path.join(__dirname,'client'),
    filename: 'bundle.js'
  },
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loaders: ['react-hot-loader', 'babel-loader?presets[]=react,presets[]=es2015']
      },
      {
        test: /\.css$/,
        loader: "style-loader!css-loader"
      }
    ]
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin()
  ]
}   ;
